extends Node


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# сигнал загрузки объектов
signal load_objekto()


var load_scene : String
var id_direktebla_query # под каким номером запроса отправили запрос на управляемый список
var id_direktebla_kosmo_query # под каким номером запроса отправили запрос на управляемый список в космосе


func _ready():
# warning-ignore:return_value_discarded
	Net.connect("connection_failed", self, "_on_connection_failed")
# warning-ignore:return_value_discarded
	Net.connect("connection_succeeded", self, "_on_connection_success")
# warning-ignore:return_value_discarded
	Net.connect("server_disconnected", self, "_on_server_disconnect")
# warning-ignore:return_value_discarded
	Net.connect("input_data", self, "_on_data_start")


# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	var q = QueryObject.new()
	id_direktebla_query = Net.current_query_id
	Net.current_query_id += 1
	Net.send_json(q.get_direktebla_json(2, 3, 2, id_direktebla_query))


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	pass


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	pass

# обработчик сигнала прихода данных при старте программы
func _on_data_start():
	var i = 0
	for on_data in Net.data_server:
		if on_data['id'] == String(id_direktebla_query):
			for objekt in on_data['payload']['data']['universoObjekto']['edges']:
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]=objekt['node']
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]['kosmo'] = false
			# теперь загружаем те объекты, которые из представленных находятся в космосе
			var q = QueryObject.new()
			id_direktebla_kosmo_query = Net.current_query_id
			Net.current_query_id += 1
			Net.send_json(q.get_direktebla_kosmo_json(id_direktebla_kosmo_query))
			#обработали запрос и удалили обработанную запись
			Net.data_server.remove(i)
		elif on_data['id'] == String(id_direktebla_kosmo_query):
			if on_data['payload'].get('data'):
				var uuid = []
				for pars in on_data['payload']['data']['filteredUniversoObjekto']['edges']:
					uuid.append(pars['node']['uuid'])
				for objekt in Global.direktebla_objekto:
					if objekt.has('uuid'):
						if objekt['uuid'] in uuid:
							objekt['kosmo']=true
				Global.loading = true
			#обработали запрос и удалили обработанную запись
			Net.data_server.remove(i)
			#отключаем перехват сигнала
			Net.disconnect("input_data", self, "_on_data_start")
# warning-ignore:return_value_discarded
			Net.connect("input_data", self, "_on_data")
			load_objektoj()
		i += 1


# обработчик прихода постоянных данных
func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
		if on_data['payload']['data'].get('filteredUniversoObjekto'):
			Global.objektoj.clear()
			var i = 0
			for item in on_data['payload']['data']['filteredUniversoObjekto']['edges']:
				if item['node']['posedantoId'] == Global.id:
					# обновляем данные directable по своему кораблю
					var kosmo = Global.direktebla_objekto[Global.realeco-2]['kosmo']
					Global.direktebla_objekto[Global.realeco-2]=item['node']
					Global.direktebla_objekto[Global.realeco-2]['kosmo'] = kosmo
				else:# свой корабль не добавляем в список
					Global.objektoj.append(item['node'])
					Global.objektoj[i]['distance'] = 0
					i += 1
			if $"/root".get_node_or_null('space'):# если загружен космос
				$"/root".get_node('space').emit_signal("load_objektoj")# загружаем объекты космоса
			emit_signal("load_objekto")
			Net.data_server.remove(i_data_server)
		i_data_server += 1


func _on_Profilo_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene('res://blokoj/profilo/profilo.tscn')


func _on_Resurso_Center_pressed():
	$CanvasLayer/UI/RCentro/Window.popup_centered()


func _on_Objektoj_pressed():
	$CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer.set_visible(true)


func set_visible(visible: bool):
	$CanvasLayer/UI.visible = visible
	

func _on_Taskoj_pressed():
	$CanvasLayer/UI/Taskoj/Window/canvas/MarginContainer.set_visible(true)

func CloseWindow():
	$CanvasLayer/UI/Taskoj/Window/canvas/MarginContainer.set_visible(false)
	$CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer.set_visible(false)
	$CanvasLayer/UI/b_itinero/itinero/canvas/MarginContainer.set_visible(false)
	$CanvasLayer/UI/interago/interago/canvas/MarginContainer.set_visible(false)


func reloadWindow():
	$CanvasLayer/UI/Taskoj/Window._on_Window_draw()
#	$CanvasLayer/UI/Objektoj/Window._on_Objekto_draw()
	load_objektoj()

	
func _on_cap_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=3:
			var mezo_cap = preload("res://kerno/menuo/resursoj/icons/tab4_3.png")
			$CanvasLayer/UI/mezo_regions.icon = mezo_cap
			$CanvasLayer/UI/real/real_cap_rect.set_visible(true)
			$CanvasLayer/UI/real/real_com_rect.set_visible(false)
			$CanvasLayer/UI/com/com_cap_rect.set_visible(true)
			$CanvasLayer/UI/com/com_real_rect.set_visible(false)
			$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
			$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
			$CanvasLayer/UI/Lbar.color = Color(0, 0.407843, 0.407843, 0.862745)
			$CanvasLayer/UI/Lbar2.color = Color(0, 0.407843, 0.407843, 0.862745)
			$CanvasLayer/UI/Lbar3.color = Color(0, 0.407843, 0.407843, 0.862745)
			$CanvasLayer/UI/real/realLabel.set_visible(false)
			$CanvasLayer/UI/com/comLabel.set_visible(false)
			$CanvasLayer/UI/cap/capLabel.set_visible(true)
			$CanvasLayer/UI/romb.color = Color(0, 0.407843, 0.407843, 0.862745)
			Global.realeco = 3
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			# если объекта не будет в космосе, то загружать станцию
			if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
# warning-ignore:return_value_discarded
				get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
			else:
# warning-ignore:return_value_discarded
				get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func _on_com_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=2:
			var mezo_com = preload("res://kerno/menuo/resursoj/icons/tab4_1.png")
			$CanvasLayer/UI/mezo_regions.icon = mezo_com
			$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
			$CanvasLayer/UI/real/real_com_rect.set_visible(true)
			$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
			$CanvasLayer/UI/com/com_real_rect.set_visible(false)
			$CanvasLayer/UI/cap/cap_com_rect.set_visible(true)
			$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
			$CanvasLayer/UI/Lbar.color = Color(0.333333, 0, 0.066667, 0.862745)
			$CanvasLayer/UI/Lbar2.color = Color(0.333333, 0, 0.066667, 0.862745)
			$CanvasLayer/UI/Lbar3.color = Color(0.333333, 0, 0.066667, 0.862745)
			$CanvasLayer/UI/real/realLabel.set_visible(false)
			$CanvasLayer/UI/com/comLabel.set_visible(true)
			$CanvasLayer/UI/cap/capLabel.set_visible(false)
			$CanvasLayer/UI/romb.color = Color(0.333333, 0, 0.066667, 0.862745)
			Global.realeco = 2
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			# если объекта не будет в космосе, то загружать станцию
			if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
# warning-ignore:return_value_discarded
				get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
			else:
# warning-ignore:return_value_discarded
				get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
#				get_tree().change_scene("res://blokoj/kosmostacioj/ComKosmostacio.tscn")
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func _on_real_pressed():
	var mezo_real = preload("res://kerno/menuo/resursoj/icons/tab4_2.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_real
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(true)
	$CanvasLayer/UI/Lbar.color = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/Lbar2.color = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/Lbar3.color = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(true)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.4, 0.6, 1, 0.862745)
	
	var reload = false
	if Global.realeco!=1:
		Global.realeco = 1
		reload = true
		# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
		CloseWindow()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://blokoj/kosmostacioj/Kosmostacio.tscn")
	if reload:
		reloadWindow()


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func komenci_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
# warning-ignore:unused_variable
	var parsed_resp = parse_json(resp)

	$request.disconnect('request_completed', self, 'komenci_request_complete')

	if $"/root".get_node_or_null('space'):
		$"/root".get_node('space').emit_signal("load_objektoj")


# ответ на заход в станцию
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_eniri_kosmostacio_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
# warning-ignore:unused_variable
	var parsed_resp = parse_json(resp)

	$request.disconnect('request_completed', self, '_on_eniri_kosmostacio_request_completed')
	
	
func _on_b_itinero_pressed():
	$CanvasLayer/UI/b_itinero/itinero/canvas/MarginContainer.set_visible(true)


func _on_ad_pressed():
	$CanvasLayer/UI/ad/ad_1/CanvasLayer/Margin.set_visible(true)


func _on_interago_pressed():
	if $CanvasLayer/UI/interago/interago/canvas/MarginContainer.visible:
		$CanvasLayer/UI/interago/interago/canvas/MarginContainer.set_visible(false)
	else:
		$CanvasLayer/UI/interago/interago.print_button()
		$CanvasLayer/UI/interago/interago/canvas/MarginContainer.set_visible(true)

func _on_eliro_button_up():
	$CanvasLayer/UI/eliro/eliro/Canvas/Popup.popup()
	$CanvasLayer/UI/eliro/eliro/Canvas/Popup.popup_centered()
	
# запрашиваем объекты
func load_objektoj():
	var q = QueryObject.new()
	Net.send_json(q.get_objekto_json( 2, 3, 2, Global.kubo))
#	$HTTPObjectoRequestFind.request(q.URL, Global.backend_headers, true, 2, q.objecto_query( 2, 3, 2, 1))




