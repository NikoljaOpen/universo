extends Control


func _on_Close_button_pressed():
	$"canvas/MarginContainer".set_visible(false)
	#закрывает окно меню
	
func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_size += event.relative
	#изменение размера окна меню

func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_position += event.relative
	#перетаскивание меню


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


