extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "profilo"

# URL к API
const URL_DATA = "https://t34.tehnokom.su/api/v1.1/"
# Необходимые заголовки
const headers = ["Content-Type: application/json"]


# Запрос на сохранение данных профиля
func save_profile_query(profile):
	return JSON.print({ "query": "mutation { redaktuUniversoUzanto(retnomo: \"%s\", publikigo: true%s) { status message universoUzanto { uuid retnomo } } }" % [profile['nickname'], profile['uuid']] })
