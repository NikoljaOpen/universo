extends WindowDialog


const QueryObject = preload("queries.gd")

var ItemListContent = []


func FillItemList():
	# Заполняет список найдеными продуктами
	for Item in ItemListContent:
		get_node("ItemList").add_item(Item, null, true)


# Вызывается перед появлением окна
func _on_Window_about_to_show():
	var q = QueryObject.new()
	
	# Делаем запрос к бэкэнду
	$HTTPProjektoRequestFind.request(q.URL, Global.backend_headers, true, 2, q.taskoj_projekto())
	$HTTPTaskojRequestFind.request(q.URL, Global.backend_headers, true, 2, q.taskoj_query())

