extends Spatial


const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")


func _on_CapKosmostacio_ready():
	# устанавливаем корабль на место 1А
	var ship = null
	ship = sxipo.instance()
	var sh = sxipo_modulo.new()
	sh.create_sxipo(ship, Global.direktebla_objekto[Global.realeco-2])

	ship.visible=true
	
#	ship.rotate_y(1.58)
	ship.rotate_y(deg2rad(-90))
	ship.translation.x = ship.translation.x + 26.4 # насколько въезжать в парковку
	ship.translation.z = ship.translation.z - 5 # в сторону от центра
	ship.translation.y = ship.translation.y + 2 # высота от пола
	add_child(ship,true)
	$camera.doni_observoj(ship)




