extends Control

onready var margin = $"/root/Title/CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer"

var index_pos = 0

func _input(event: InputEvent) -> void:
	
	if Input.is_action_just_pressed("right_click"):
#		event.pressed=true
		if event is InputEventMouseButton and $"/root/Title/CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer".visible:
			$canvas/PopupMenu.set_item_disabled(2,true)
#			$canvas/PopupMenu.mouse_filter=2
			var x = $".".get_global_mouse_position().x
			var y = $".".get_global_mouse_position().y
			if (margin.margin_top<y) and (y<margin.margin_bottom) and (margin.margin_left<x) and (x<margin.margin_right):
				$canvas/PopupMenu.margin_left=x
				$canvas/PopupMenu.margin_top=y
				$canvas/PopupMenu.visible=true
#				#если пункт меню - станция
				x = $"/root/Title/CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".get_local_mouse_position().x
				y = $"/root/Title/CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".get_local_mouse_position().y
				index_pos = $"/root/Title/CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".get_item_at_position(Vector2(x,y),true)
				$"/root/Title/CanvasLayer/UI/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".select(index_pos)
				if Global.objektoj[index_pos]['resurso']['objId'] == 1:#объект станция Espero
					#проверяем как далеко от станции и если менее 20, то разрешаем войти
					var dist = $"../ship".translation.distance_to(Vector3(
						Global.objektoj[index_pos]['koordinatoX'],
						Global.objektoj[index_pos]['koordinatoY'],
						Global.objektoj[index_pos]['koordinatoZ']
					))
					if dist<400:
						$canvas/PopupMenu.set_item_disabled(2,false)
				

# сдвиг по всем координатам по целеполаганию полёта к объекту
const translacio = 20
const translacio_stat = 300

const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# вход в станцию
func go_kosmostacioj():
	# Разрегистрируем обработчик сигнала request_completed (вызывается
	# по завершении HTTPRequest)
# warning-ignore:return_value_discarded
	Title.get_node("request").connect('request_completed', Title, '_on_eniri_kosmostacio_request_completed')
	var q = QueryObject.new()
	# закрываем проект
	#  добавляем запись в связи, что находимся внутри
	var uuid_tasko = ''
	if $"../ship".projekto_uuid:
		uuid_tasko = Global.itineroj[0]['uuid_tasko']
	var error = Title.get_node("request").request(q.URL_DATA, 
		Global.backend_headers,
		true, 2, q.eniri_kosmostacio(
			$"../ship".projekto_uuid,
			uuid_tasko, 
			Global.objektoj[index_pos]['uuid']))
	# Если запрос не выполнен из-за какой-то ошибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in GET (_on_eniri_kosmostacio_request_completed) Request.')
	# добавляем в данные пользователя о станции для последующего выхода
	if !Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
		Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo'] = {'edges':[]}
	Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].append({
		'node': {'posedanto': {'koordinatoX' : Global.objektoj[index_pos]['koordinatoX'],
		'koordinatoY' : Global.objektoj[index_pos]['koordinatoY'],
		'koordinatoZ' : Global.objektoj[index_pos]['koordinatoZ'],
		'kubo': {'objId' : Global.kubo},},
		'uuid' : Global.objektoj[index_pos]['uuid']},})
	Title.CloseWindow()
	Global.direktebla_objekto[Global.realeco-2]['kosmo'] = false
	# вызываем сцену станции
# warning-ignore:return_value_discarded
	get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')


func _on_PopupMenu_index_pressed(index):
	if index == 2: # если выбран вход в станцию
		go_kosmostacioj()
	elif index == 3: # если выбрана стрельба по объекту
		# вычисляем объект в космосе
		# проходим по всем созданным объектам в космосе и находим нужный по uuid
		for child in $"../".get_children():
			if child.is_in_group('create'):
				if child.uuid == Global.objektoj[index_pos]['uuid']:
#					for ch in $"../ship/CollisionShape".get_children():
#						print('==',ch.name)
#					$"../ship/CollisionShape/laser_gun".set_target(child.get_global_transform())
					$"../ship/laser_gun".set_target(child)
	else: # если выбрано движение к цели или добавление в маршрут
		# вычисляем точку в пространстве, придвинутую на translacio ближе
		# если станция, то дистанция больше
		var celo = Vector3(Global.objektoj[index_pos]['koordinatoX'],
			Global.objektoj[index_pos]['koordinatoY'],
			Global.objektoj[index_pos]['koordinatoZ'])
		
		var dist = 0
		if Global.objektoj[index_pos]['resurso']['objId']==1:
			dist = $"../ship".translation.distance_to(celo) - translacio_stat
		else:
			dist = $"../ship".translation.distance_to(celo) - translacio
		var speed = celo - $"../ship".translation
		celo = $"../ship".translation + speed.normalized() * dist

		Global.fenestro_itinero.add_itinero(
			'',
			Global.objektoj[index_pos]['uuid'],
			Global.objektoj[index_pos]['nomo']['enhavo'], #'nomo'
			celo.x,
			celo.y,
			celo.z,
			dist
		)
		Global.fenestro_itinero.FillItemList()
		if index==0:
			Global.fenestro_itinero.komenci_itinero()
	#	elif index==1:


func _on_PopupMenu_draw():
#	$canvas/PopupMenu.mouse_filter=0 #для отключения игнорирования меню и его реакции на выбор пунктов
	pass # Replace with function body.

