extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Космос"


# URL к API
const URL = "https://t34.tehnokom.su/api/v1.1/"


# задаём координаты и угол поворота объекту
func objecto_mutation(uuid, koordX, koordY, koordZ, rotaciaX, rotaciaY, rotaciaZ):
	return JSON.print({ 'query': 'mutation ($uuid:UUID, $koordX:Float, $koordY:Float, $koordZ:Float,'+
		' $rotaciaX:Float, $rotaciaY:Float, $rotaciaZ:Float )'+
		' { redaktuUniversoObjekto ( uuid: $uuid, koordinatoX: $koordX, koordinatoY: $koordY, '+
		'koordinatoZ: $koordZ, rotaciaX: $rotaciaX, rotaciaY: $rotaciaY, rotaciaZ: $rotaciaZ ) '+
		' { status message universoObjektoj { uuid } } }',
		'variables': {"uuid":uuid, "koordX": koordX, "koordY": koordY, "koordZ": koordZ,
		"rotaciaX": rotaciaX, "rotaciaY": rotaciaY, "rotaciaZ": rotaciaZ} })


# устанавливаем проект
func instalo_projekto(objektoUuid, kom_koordX, kom_koordY, kom_koordZ,
	fin_koordX, fin_koordY, fin_koordZ):
	var tipoId = 2
	var kategorio = 3
	var statusoId = 2
	var nomo = "Movado"
	var priskribo = "Movado de objekto"
	var statusoPosedantoId = 1
	var tipoPosedantoId = 1
	var query = JSON.print({ 'query': 'mutation ($tipoId:Int, $kategorio:[Int],'+
		' $nomo:String, $priskribo:String, $statusoId:Int, $kom_koordX:Float, '+
		' $kom_koordY:Float, $kom_koordZ:Float, $fin_koordX:Float, '+
		' $fin_koordY:Float, $fin_koordZ:Float, $tipoPosedantoId:Int,'+
		' $statusoPosedantoId:Int, $objektoUuid:String, '+
		' $realecoId:Int ) '+
		'{ redaktuUniversoProjekto ( '+
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$kom_koordX,'+
		' komKoordinatoY:$kom_koordY, komKoordinatoZ:$kom_koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$tipoPosedantoId, '+
		' objektoUuid: $objektoUuid, posedantoStatusoId:$statusoPosedantoId ,'+
		' realecoId:$realecoId ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo,
			"priskribo": priskribo, "statusoId": statusoId,
			"kom_koordX": kom_koordX, "kom_koordY": kom_koordY, "kom_koordZ": kom_koordZ, 
			"fin_koordX":fin_koordX, 
			"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
			"objektoUuid":objektoUuid, "statusoPosedantoId":statusoPosedantoId,
			"tipoPosedantoId":tipoPosedantoId, "realecoId":Global.realeco} })
	# print('===instalo_projekto===',query)
	return query

# записываем список задач с их владельцами и текущие координаты объекту
func instalo_tasko_posedanto_koord(uuid, projekto_uuid, kom_koordX, kom_koordY, kom_koordZ, itineroj):
	# создаём список задач, создаём владельца проекта, устанавливаем координаты объекту
	var tipoId = 2
	var kategorio = 3
	var statusoId = [2]
	var nomo = "Movado"
	var priskribo = "Movado de objekto"
	var tipoPosedantoId = 1
	var statusoPosedantoId = 1
	var pozicio =[1]
	var fin_koordX = []
	var fin_koordY = []
	var fin_koordZ = []
	# параметры координат последующих задач
	var i = 0
	var komKoordinatoX=[]
	var komKoordinatoY=[]
	var komKoordinatoZ=[]
	for iti in itineroj:
		if i==0:
			komKoordinatoX.append(kom_koordX)
			komKoordinatoY.append(kom_koordY)
			komKoordinatoZ.append(kom_koordZ)
		else:
			pozicio.append(i+1)
			komKoordinatoX.append(itineroj[i-1]['koordinatoX'])
			komKoordinatoY.append(itineroj[i-1]['koordinatoY'])
			komKoordinatoZ.append(itineroj[i-1]['koordinatoZ'])
			statusoId.append(1)
		fin_koordX.append(itineroj[i]['koordinatoX'])
		fin_koordY.append(itineroj[i]['koordinatoY'])
		fin_koordZ.append(itineroj[i]['koordinatoZ'])
		i += 1

	var query = JSON.print({ 'query': 'mutation ($uuid:UUID, $koordX:Float, $koordY:Float, $koordZ:Float, ' +
		'$tipoId:Int, $kategorio:[Int], $nomo:String, $priskribo:String, $statusoId:[Int], $projekto_uuid: String,' +
		'$komKoordinatoX:[Float], $komKoordinatoY:[Float], $komKoordinatoZ:[Float], $tipoPosedantoId:Int,' +
		'$fin_koordX:[Float], $fin_koordY:[Float], $fin_koordZ:[Float], $pozicio:[Int], $statusoPosedantoId:Int,' +
		'$objektoUuid:String, $realecoId:Int)'+
		' { redaktuUniversoObjekto ( uuid: $uuid, koordinatoX: $koordX, koordinatoY: $koordY, '+
		' koordinatoZ: $koordZ, realecoId:$realecoId ) { status message universoObjektoj { uuid } } '+
		'redaktuKreiUniversoTaskojPosedanto (projektoUuid: $projekto_uuid, tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, pozicio:$pozicio, komKoordinatoX:$komKoordinatoX,'+
		' komKoordinatoY:$komKoordinatoY, komKoordinatoZ:$komKoordinatoZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoStatusoId:$statusoPosedantoId,'+
		' posedantoTipoId:$tipoPosedantoId, objektoUuid:$objektoUuid, '+
		' realecoId:$realecoId ) { status '+
		' message universoTaskoj { uuid } } }',
		'variables':  {"uuid":uuid, "koordX": kom_koordX, "koordY": kom_koordY, "koordZ": kom_koordZ,
		"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "projekto_uuid": projekto_uuid, "fin_koordX":fin_koordX, 
		"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ, "tipoPosedantoId":tipoPosedantoId,
		"statusoPosedantoId":statusoPosedantoId, "objektoUuid":uuid, "pozicio":pozicio,
		"komKoordinatoX":komKoordinatoX, "komKoordinatoY":komKoordinatoY, "komKoordinatoZ":komKoordinatoZ,
		"realecoId":Global.realeco}})
	# print('===instalo_tasko_posedanto_koord===',query)
	return query


# создаём задачу, устанавливаем координаты объекту, изменяем финальные координаты проекту
func instalo_tasko_koord(uuid, projekto_uuid, kom_koordX, kom_koordY, kom_koordZ,
		fin_koordX, fin_koordY, fin_koordZ):
	var posedantoTipoId = 1
	var posedantoStatusoId = 1
	var tipoId = 2
	var kategorio = 3 # категория движения объектов в космосе
	var statusoId = 2
	var nomo = "Movado"
	var priskribo = "Movado de objekto"
	var query = JSON.print({ 'query': 'mutation ($uuid:UUID, $koordX:Float, $koordY:Float, $koordZ:Float, '+
		'$tipoId:Int, $kategorio:[Int], $nomo:String, $priskribo:String, $statusoId:Int, $projekto_uuid: UUID,'+
		'$fin_koordX:Float, $fin_koordY:Float, $fin_koordZ:Float, $projektoUuid: String, '+
		'$posedantoTipoId:Int, $posedantoStatusoId:Int, $objektoUuid:String, '+
		'$realecoId:Int)'+
		'{ redaktuUniversoObjekto ( uuid: $uuid, koordinatoX: $koordX, koordinatoY: $koordY, '+
		' koordinatoZ: $koordZ, realecoId:$realecoId ) { status message universoObjektoj { uuid } } '+
		'redaktuUniversoTaskoj (projektoUuid: $projektoUuid, tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$koordX,'+
		' komKoordinatoY:$koordY, komKoordinatoZ:$koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$posedantoTipoId, '+
		' objektoUuid:$objektoUuid, posedantoStatusoId:$posedantoStatusoId, '+
		' realecoId:$realecoId) '+
		'{ status '+
		' message universoTaskoj { uuid } } '+
		'redaktuUniversoProjekto ( uuid:$projekto_uuid ,'+
		' finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"uuid":uuid, "koordX": kom_koordX, "koordY": kom_koordY, "koordZ": kom_koordZ,
		"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "projekto_uuid": projekto_uuid, "projektoUuid": projekto_uuid, 
		"fin_koordX":fin_koordX, "fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
		"posedantoStatusoId":posedantoStatusoId, "posedantoTipoId":posedantoTipoId,
		"objektoUuid":uuid, "realecoId":Global.realeco } })
	# print('===instalo_tasko_koord===',query)
	return query


# завершение задачи
func finado_tasko(tasko_uuid, statusoId = 4):
	return JSON.print({ 'query': 'mutation ($uuid:UUID,  '+
		' $statusoId:Int, )'+
		'{ redaktuUniversoTaskoj (uuid: $uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoTaskoj { uuid } } }',
		'variables': {"uuid":tasko_uuid, "statusoId": statusoId } })


# завершение задачи и проекта
func finado_projeko_tasko(projekto_uuid, tasko_uuid):
	var statusoId = 4
	return JSON.print({ 'query': 'mutation ($tasko_uuid:UUID, $projekto_uuid:UUID, '+
		' $statusoId:Int, )'+
		'{ redaktuUniversoTaskoj (uuid: $tasko_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoTaskoj { uuid } } '+
		'redaktuUniversoProjekto (uuid: $projekto_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoProjekto { uuid } } '+
		'}',
		'variables': {"tasko_uuid":tasko_uuid, "statusoId": statusoId, "projekto_uuid":projekto_uuid } })


# завершение проекта
func finado_projeko_json(projekto_uuid, id=0):
	var statusoId = 4 # Закрыт 
	if !id:
		id = Net.current_query_id
		Net.current_query_id += 1
	return JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($projekto_uuid:UUID, '+
		' $statusoId:Int, )'+
		'{ redaktuUniversoProjekto (uuid: $projekto_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoProjekto { uuid } } '+
		'}',
		'variables': {"statusoId": statusoId, "projekto_uuid":projekto_uuid } }})

# подписка на действия в кубе
func kubo_json(id=0):
	if !id:
		id = Net.current_query_id
		Net.current_query_id += 1
	var kategorio = 3 # категория движения объектов в космосе
	return JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription ($kuboj:[Int]!, $realeco:Int!, $kategorio:Int!)'+
		'{ universoObjektoEventoj (kuboj: $kuboj, realeco:$realeco, kategorio: $kategorio) { evento '+
		' objekto { uuid koordinatoX koordinatoY koordinatoZ} '+
		' projekto {uuid} '+
		' tasko { uuid komKoordinatoX komKoordinatoY '+
		' komKoordinatoZ finKoordinatoX finKoordinatoY '+
		' finKoordinatoZ pozicio statuso{objId} kategorio { '+
		' edges { node { objId } } }} } '+
		'}',
		'variables': {"kuboj": Global.kubo, "realeco": Global.realeco, "kategorio":kategorio } }})


func test_json(id=0):
	if !id:
		id = Net.current_query_id
		Net.current_query_id += 1
	return JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription '+
	'{ mesagxiloEventoj (babilejoj: [1,2]) { ' +
	'evento ' +
	'babilejo { uuid } } }'}})


