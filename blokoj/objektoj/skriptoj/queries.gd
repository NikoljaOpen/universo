extends Object
# Здесь будем хранить всё для запросов к бэкэнду 


# URL к API
const URL = "https://t34.tehnokom.su/api/v1.1/"


# Запрос к API, выбираем объекты, которые в космосе
func objecto_query(statusoId, kategorioId, tipoId, kuboId = 1):
	var query = JSON.print({ "query": "query ($kuboId:Float, $statusoId:Float, "+
		" $realecoId:Float, $kategorioId:Float, $tipoId:Float) " +
		"{ filteredUniversoObjekto (realeco_Id:$realecoId, kubo_Id: $kuboId, "+
		" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
			" ) { edges { node { uuid posedantoId "+
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  tasko (kategorio_Id:$kategorioId){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ statuso {objId} } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" nomo{enhavo}" +
		" ligilo{edges{node{ "+
		"  konektiloPosedanto konektiloLigilo tipo{objId}		"+
		"  ligilo{ uuid nomo{enhavo} integreco resurso{objId} "+
		"   ligilo{edges{node{" +
		"    konektiloPosedanto konektiloLigilo tipo{objId} " +
		"    ligilo{ uuid integreco resurso{objId} }}}}}" +
		"    tipo{objId}}}}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"kuboId":kuboId, "statusoId":statusoId, 
		"kategorioId":kategorioId, "tipoId":tipoId,
		"realecoId":Global.realeco} })
	# print('===objecto_query=',query)
	return query
