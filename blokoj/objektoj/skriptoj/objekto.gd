extends "res://kerno/fenestroj/tipo1.gd"

const QueryObject = preload("queries.gd")


func _ready():
	Title.connect("load_objekto", self, "_reload_objekto")

# перезагружаем список объектов
func _reload_objekto():
	$"canvas/MarginContainer/VBoxContainer/scroll/ItemList".clear()
	FillItemList()

func FillItemList():
	# если загружен космос, то берём список из объектов космоса
	if $"/root".get_node_or_null('space'):
		var _space = $"/root".get_node('space')
		var _items = get_node("canvas/MarginContainer/VBoxContainer/scroll/ItemList")
		for ch in _space.get_children():
			if ch.is_in_group('create') and ('objekto' in ch):
				_items.add_item(
					'('+String(int(
					_space.get_node('ship').translation.distance_to(ch.translation))
					)+') '+ch.objekto['nomo']['enhavo'], 
					null, 
					true
				)
	else:
	# Заполняет список найдеными объектами
		for item in Global.objektoj:
			get_node("canvas/MarginContainer/VBoxContainer/scroll/ItemList").add_item(
				'('+String(int(item['distance']))+') '+item['nomo']['enhavo'], 
				null, 
				true
			)


func distance_to(trans):
#	for obj in ItemListContent:
	for obj in Global.objektoj:
		obj['distance'] = trans.distance_to(Vector3(obj['koordinatoX'],
			obj['koordinatoY'],obj['koordinatoZ']))
	$'canvas/MarginContainer/VBoxContainer/scroll/ItemList'.clear()
	FillItemList()


